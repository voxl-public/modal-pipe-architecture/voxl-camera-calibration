/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef THRESHOLDING_H_
#define THRESHOLDING_H_

#include <stdint.h>

/**
 * 
 * Apply basic cutoff thresholding to an image
 * 
 * @param w width of image
 * @param h height of image
 * @param im input image
 * @param threshim return output image, up to caller to allocate memory
 * 
 * @return the cutoff value used
 */
uint8_t mcv_threshold_fixed(const int w, const int h, const uint8_t *im, uint8_t *threshim, uint8_t cutoff);

/**
 * Apply basic average threshold to an image. will average all pixels in the
 * region of the image from (x,y) to (x+w, y+h) and use this avg as a simple
 * cutoff.
 *
 * @param      im_w      width of entire image
 * @param      im_h      height of entire image
 * @param      x         starting x coordinate of partial threshold
 * @param      y         starting y coordinate of partial threshold
 * @param      w         width of partial section (starting at x)
 * @param      h         height of partial section (starting at y)
 * @param      im        input image
 * @param      threshim  return output image, up to caller to allocate memory
 *
 * @return the cutoff value used
 */
uint8_t mcv_threshold_mean_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t* threshim, int adjustment);

/**
 * 
 * Apply Adaptive Thresholding to an image
 * 
 * @param w width of image
 * @param h height of image
 * @param im input image
 * @param threshim return output image, up to caller to allocate memory
 * 
 */

void mcv_threshold_tile(const int w, const int h, const uint8_t *im, uint8_t *threshim);

/**
 * Apply Adaptive Thresholding to a partial section of an image. Uses a grid of
 * tiles and averages vals with a small convultion between the tiles. Will
 * threshold the image from (x,y) to (x+w, y+h), so if c = y = 0, and w = im_w,
 * h = im_h, the entire image will be thresholded.
 *
 * @param      im_w        width of entire image
 * @param      im_h        height of entire image
 * @param      x           starting x coordinate of partial threshold
 * @param      y           starting y coordinate of partial threshold
 * @param      w           width of partial section (starting at x)
 * @param      h           height of partial section (starting at y)
 * @param      im          input image
 * @param      threshim    return output image, up to caller to allocate memory
 * @param[in]  adjustment  adjust the threshold value from the auto calculated value up or down by this
 */
void mcv_threshold_tile_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment);



/**
 * threshold based on the median of the region
 * @return the cutoff value used
 */
uint8_t mcv_threshold_median_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment);


/**
 * similar to Otsu's method, it finds the threshold in between two peaks
 * in the histogram such that it reduces the variance in the two groups
 **/
uint8_t mcv_threshold_otsu_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment);

// like above but choosed an appropriate 3x3, 3x2, or 3x1 grid
uint8_t mcv_threshold_otsu_tile_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment);

// like above but interpolates that grid over the full resolution
uint8_t mcv_threshold_otsu_tile_partial_interpolated(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment);


// just a wrapper for easy swapping between algos
uint8_t mcv_threshold_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment);


#endif
