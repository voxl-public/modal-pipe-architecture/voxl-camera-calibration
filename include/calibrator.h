/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef CALIBRATOR_H_
#define CALIBRATOR_H_


#include <stdint.h>
#include <opencv2/core/types.hpp>
#include <string>
#include <modal_pipe.h>

#define OVERLAY_CH          0
#define OVERLAY_NAME        "camera_calibrator_overlay"
#define OVERLAY_LOCATION    OVERLAY_NAME

#define CAMERA_CH           3
#define PROCESS_NAME        "voxl-camera-calibrator"

#ifdef __ANDROID__
    #define DATA_ROOT       "/data/local/tmp/"
    #define PIPE_SIZE       1024*1024*1;                        // 1MB limit
#else
    #define DATA_ROOT       "/data/modalai/"
    #define PIPE_SIZE       1024*1024*128;                       // size_bytes
#endif // __ANDROID__

#define GREEN               cv::Scalar(76, 216, 72)
#define RED                 cv::Scalar(255, 0, 0)
#define BLUE                cv::Scalar(37, 142, 166)
#define WHITE               cv::Scalar(255, 255, 255)
#define BLACK               cv::Scalar(0, 0, 0)

/**
 * board_params
 * contains info specific to each chessboard detection
**/
typedef struct board_params {
    float p_x       = 0;
    float p_y       = 0;
    float p_size    = 0;
    float p_skew    = 0;
} board_params;


/**
 * calib_params
 * contains parameters for calibration functions
 * used for both stereo and mono calibrations
**/
typedef struct calib_params {
    cv::Size board_size            = cv::Size(0,0);    // The size of the board -> Number of items by width and height
    float    square_size           = 0;                // The size of a square in meters.
    bool     use_fisheye           = false;            // use fisheye camera model for calibration
    bool     is_mono               = false;            // Mono or stereo
} calib_params;



bool isDoneSampling();

/**
 * validate_params
 * given a calib_params struct, will validate that all user updated data is valid
 * also updates flag variable in calib_params with correct opencv flags based on set bools
**/
bool validate_params(calib_params cp);


// Helper function to strip known tags
std::string strip_tags(const std::string &input_pipe);

/**
 * run_mono_calibration
 * runs the mono camera calibration and saves it if everything runs smoothly
 * calls internal functions run_calibration_mono and save_camera_params_mono
**/
bool run_mono_calibration_and_save(calib_params cp, float grid_width, std::vector<std::vector<cv::Point2f>> imagePoints);

/**
 * run_stereo_calibration
 * runs the stereo camera calibration and saves it if everything runs smoothly
 * calls internal functions run_calibration_stereo and save_camera_params_stereo
**/
bool run_stereo_calibration_and_save(calib_params cp, float grid_width, std::vector<std::vector<cv::Point2f>>* imagePoints);
 
void *ConsumerThreadFunc(void *data);

void signal_consumer(void);

void set_result(int passed);

#endif
