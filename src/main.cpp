/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>  // for fprintf
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include "calibrator.h"
#include <pthread.h>
#ifndef __ANDROID__
#include <opencv4/opencv2/core/types.hpp>
#include <opencv4/opencv2/core/utils/logger.hpp>
#endif // __ANDROID__
calib_params cp;
char input_pipe[MODAL_PIPE_MAX_DIR_LEN];

bool use_thresh_overlay   = true;
bool extrinsics_only      = false;
bool skip_extrinsics      = false;
bool mirror_image         = true;
bool no_target_extrinsics = false;
bool thermal_cal          = false;
bool en_debug             = false;

std::string board_size;
std::string delim = "x";
std::string token;
size_t pos = 0;

// -----------------------------------------------------------------------------------------------------------------------------
// Print the help message
// -----------------------------------------------------------------------------------------------------------------------------
static void _print_usage()
{
    printf("First arg must be the name of a camera pipe\n");
    printf("Subsequent arguments are as follows:\n\n");
    printf("-d, --en_debug         : enable debug prints\n");
    printf("-e, --extrinsics-only  : Pull an existing intrinsics file and only calibrate extrinsics (stereo only)\n");
    printf("-f, --fisheye          : Use the fisheye lens distortion mode\n");
    printf("-g, --skip-extrinsics  : skip calibrating extrinsics for stereo cameras\n");
    printf("-h, --help             : Print this help message\n");
    printf("-l, --length           : Length of square edge in meters (default 0.0655 m)\n");
    printf("-m, --mirror           : Mirror the overlay output\n");
    printf("-n, --no-targets       : Do the stereo extrinsics calibration with user selected images instead of targets\n");
    printf("-r, --no-threshold     : Disable thresholded image filter in overlay (does not change functionality)\n");
    printf("-s, --size NxM         : Dimensions of inner points in the board (NxM) (default 5x6)\n");
    printf("-z, --thermal          : Do a special calibration process for a thermal camera like Flir Lepton\n");
    printf("\nExamples:\n");
    printf("voxl-calibrate-camera tracking --fisheye\n");
    printf("voxl-calibrate-camera stereo\n");
    printf("voxl-calibrate-camera lepton0_raw --thermal\n");
}

// -----------------------------------------------------------------------------------------------------------------------------
// Parses the command line arguments to the main function
// -----------------------------------------------------------------------------------------------------------------------------
static int _parse_opts(int         argc,           ///< Number of arguments
                      char* const argv[])          ///< arguments
{
    static struct option LongOptions[] =
    {
        {"en_debug",        no_argument,        0, 'd'},
        {"extrinsics-only", no_argument,        0, 'e'},
        {"fisheye",         no_argument,        0, 'f'},
        {"skip-extrinsics", no_argument,        0, 'g'},
        {"help",            no_argument,        0, 'h'},
        {"length",          required_argument,  0, 'l'},
        {"mirror",          no_argument,        0, 'm'},
        {"no-targets",      no_argument,        0, 'n'},
        {"no-threshold",    no_argument,        0, 'r'},
        {"size",            required_argument,  0, 's'},
        {"thermal",         no_argument,        0, 'z'},
    };

    int optionIndex      = 0;
    int status           = 0;
    int option;

    // Current defaults for the cards around the office
    cp.board_size.width  = 5;
    cp.board_size.height = 6;
    cp.square_size       = 0.0655;
    cp.use_fisheye = false;

    while ((status == 0) && (option = getopt_long (argc, argv, "defghl:nmrs:tz", &LongOptions[0], &optionIndex)) != -1)
    {
        switch(option)
        {
            case 'd':
                en_debug = true;
                break;

            case 'e':
                if(skip_extrinsics){
                    fprintf(stderr, "You can simultaneously request to cal extrinsics only and skip extrinsics\n");
                    return -1;
                }
                extrinsics_only = true;
                break;

            case 'f':
                cp.use_fisheye = true;
                break;

            case 'g':
                if(extrinsics_only){
                    fprintf(stderr, "You can simultaneously request to cal extrinsics only and skip extrinsics\n");
                    return -1;
                }
                skip_extrinsics = true;
                break;

            case 'h':
                status = -1;
                break;

            case 'l':
                // length of a single edge, float 0.0655
                cp.square_size = atof(optarg);
                break;

            case 'm':
                mirror_image = false;
                break;

            case 'n':
                no_target_extrinsics = true;
                break;

            case 'r':
                use_thresh_overlay = false;
                break;

            case 's':
                // NxM
                board_size.assign(optarg);
                while ((pos = board_size.find(delim)) != std::string::npos) {
                    token = board_size.substr(0, pos);
                    cp.board_size.width = std::stoi(token);
                    board_size.erase(0, pos+1);
                    cp.board_size.height = std::stoi(board_size);
                }
                break;

            case 't':
                printf("ERROR the -t (--threshold) argument is no longer valid\n");
                printf("This mode is enabled by default, there is no need to use this flag\n");
                exit(-1);
                break;

            case 'z':
                printf("Enabling thermal calibration\n");
                thermal_cal = true;
                break;

            // Unknown argument
            case '?':
            default:
                printf("Invalid argument passed!\n");
                status = -1;
                break;
        }
    }

    // scan through the non-flagged arguments for the desired pipe
    for(int i=optind; i<argc; i++){
        if(input_pipe[0]!=0){
            fprintf(stderr, "ERROR: Please specify only one pipe\n");
            _print_usage();
            exit(-1);
        }

        strcpy(input_pipe, argv[i]);
    }

    if(input_pipe[0]==0){
        fprintf(stderr, "ERROR: first argument must be a camera pipe\n");
        _print_usage();
        exit(-1);
    }

    return status;
}

int main(int argc, char* const argv[]){
    if(_parse_opts(argc, argv)){
        _print_usage();
        return -1;
    }

    if (!validate_params(cp)){
        fprintf(stderr, "Invalid params\n");
        _print_usage();
        return -1;
    }
    float grid_width = cp.square_size * (cp.board_size.width - 1);

    // init extended pipe
    int flags = 0;
    pipe_info_t info;
    strncpy(info.name,       OVERLAY_NAME, MODAL_PIPE_MAX_NAME_LEN); // name
    strncpy(info.location,   OVERLAY_LOCATION, MODAL_PIPE_MAX_DIR_LEN);// location
    strcpy(info.type,        "camera_image_metadata_t");   // type
    strcpy(info.server_name, PROCESS_NAME);                // server_name
    info.size_bytes = PIPE_SIZE;
    //Setup the output overlay pipe
    if(pipe_server_create(OVERLAY_CH, info, flags)){
        return -1;
    }

    std::vector<std::vector<cv::Point2f>> imagePoints[4];

    enable_signal_handler();
    main_running = 1;

    pthread_t consumerThread;
    pthread_attr_t consumerAttr;
    pthread_attr_init(&consumerAttr);
    pthread_attr_setdetachstate(&consumerAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&consumerThread, &consumerAttr, ConsumerThreadFunc, &imagePoints);
    pthread_attr_destroy(&consumerAttr);

    // keep going until the  signal handler sets the running flag to 0
    while(!isDoneSampling() && main_running) usleep(500000);
    if(en_debug) printf("done sampling\n");
    int retval;
    if(main_running){// Did not get aborted by sigint, run calibration

        struct stat st = {0};
        if (stat(DATA_ROOT, &st) == -1) {
            printf("Could not find folder: %s, making one\n", DATA_ROOT);
            mkdir(DATA_ROOT, 0777);
        }

        printf("Running Calibration...\n");
        if(cp.is_mono){
            run_mono_calibration_and_save(cp, grid_width, imagePoints[0]);
        } else {
            run_stereo_calibration_and_save(cp, grid_width, imagePoints);
        }
        retval = 0;
    } else retval = 1;


    // let the image processing thread send some images reporting the result
    // before we shut everything down
    usleep(500000);
    main_running = 0;

    if(en_debug) printf("waiting to join consumer thread\n");
    signal_consumer();
    pthread_join(consumerThread, NULL);

    // all done, signal pipe read threads to stop
    return retval;

}
